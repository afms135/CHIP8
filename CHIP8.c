#include "CHIP8.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

static int op0(CHIP8 *state, uint16_t op);
static int op1(CHIP8 *state, uint16_t op);
static int op2(CHIP8 *state, uint16_t op);
static int op3(CHIP8 *state, uint16_t op);
static int op4(CHIP8 *state, uint16_t op);
static int op5(CHIP8 *state, uint16_t op);
static int op6(CHIP8 *state, uint16_t op);
static int op7(CHIP8 *state, uint16_t op);
static int op8(CHIP8 *state, uint16_t op);
static int op9(CHIP8 *state, uint16_t op);
static int opA(CHIP8 *state, uint16_t op);
static int opB(CHIP8 *state, uint16_t op);
static int opC(CHIP8 *state, uint16_t op);
static int opD(CHIP8 *state, uint16_t op);
static int opE(CHIP8 *state, uint16_t op);
static int opF(CHIP8 *state, uint16_t op);

static uint8_t font[] =
{
	0xF0, 0x90, 0x90, 0x90, 0xF0,	//0
	0x20, 0x60, 0x20, 0x20, 0x70,	//1
	0xF0, 0x10, 0xF0, 0x80, 0xF0,	//2
	0xF0, 0x10, 0xF0, 0x10, 0xF0,	//3
	0x90, 0x90, 0xF0, 0x10, 0x10,	//4
	0xF0, 0x80, 0xF0, 0x10, 0xF0,	//5
	0xF0, 0x80, 0xF0, 0x90, 0xF0,	//6
	0xF0, 0x10, 0x20, 0x40, 0x40,	//7
	0xF0, 0x90, 0xF0, 0x90, 0xF0,	//8
	0xF0, 0x90, 0xF0, 0x10, 0xF0,	//9
	0xF0, 0x90, 0xF0, 0x90, 0x90,	//A
	0xE0, 0x90, 0xE0, 0x90, 0xE0,	//B
	0xF0, 0x80, 0x80, 0x80, 0xF0,	//C
	0xE0, 0x90, 0x90, 0x90, 0xE0,	//D
	0xF0, 0x80, 0xF0, 0x80, 0xF0,	//E
	0xF0, 0x80, 0xF0, 0x80, 0x80,	//F
};

static uint8_t bigfont[] =
{
	0x7C, 0xFE, 0xC6, 0xC6, 0xC6, 0xC6, 0xC6, 0xFE, 0x7C, 0x00,	//0
	0x18, 0x38, 0x78, 0x18, 0x18, 0x18, 0x18, 0x7E, 0x7E, 0x00,	//1
	0x7C, 0xFE, 0xC6, 0x06, 0x3E, 0x7C, 0xC0, 0xFE, 0xFE, 0x00,	//2
	0x7C, 0xFE, 0xC6, 0x06, 0x3C, 0x06, 0xC6, 0xFE, 0x7C, 0x00,	//3
	0xC6, 0xC6, 0xC6, 0xC6, 0xFE, 0x7E, 0x06, 0x06, 0x06, 0x00,	//4
	0xFE, 0xFE, 0xC0, 0xFC, 0xFE, 0x06, 0xC6, 0xFE, 0x7C, 0x00,	//5
	0x7C, 0xFE, 0xC6, 0xC0, 0xFC, 0xC6, 0xC6, 0xFE, 0x7C, 0x00,	//6
	0xFE, 0xFE, 0x06, 0x0E, 0x1C, 0x38, 0x30, 0x30, 0x30, 0x00,	//7
	0x7C, 0xFE, 0xC6, 0xC6, 0x7C, 0xC6, 0xC6, 0xFE, 0x7C, 0x00,	//8
	0x7C, 0xFE, 0xC6, 0xC6, 0x7E, 0x06, 0xC6, 0xFE, 0x7C, 0x00,	//9
	0x7C, 0xFE, 0xC6, 0xC6, 0xFE, 0xFE, 0xC6, 0xC6, 0xC6, 0x00,	//A
	0xFC, 0xFE, 0xC6, 0xC6, 0xFC, 0xC6, 0xC6, 0xFE, 0xFC, 0x00,	//B
	0x7C, 0xFE, 0xC6, 0xC0, 0xC0, 0xC0, 0xC6, 0xFE, 0x7C, 0x00,	//C
	0xF8, 0xFC, 0xCE, 0xC6, 0xC6, 0xC6, 0xCE, 0xFC, 0xF8, 0x00,	//D
	0xFE, 0xFE, 0xC0, 0xC0, 0xFC, 0xC0, 0xC0, 0xFE, 0xFE, 0x00,	//E
	0xFE, 0xFE, 0xC0, 0xC0, 0xFC, 0xFC, 0xC0, 0xC0, 0xC0, 0x00,	//F
};

int CHIP8_init(CHIP8 *state)
{
	srand((unsigned int)time(NULL));	//Seed RNG
	memset(state, 0, sizeof(CHIP8));

	state->mem = (uint8_t*)malloc(4096);
	if (state->mem == NULL)
		return CHIP8_MALLOC;

	memcpy(state->mem, font, sizeof(font));	//Load font at 0x000
	memcpy(state->mem + sizeof(font), bigfont, sizeof(bigfont));//Load 10-byte font
	state->PC = 0x200;
	return CHIP8_OK;
}

void CHIP8_reset(CHIP8 *state)
{
	uint8_t *save = state->mem;
	uint8_t flags = state->flags;
	memset(state, 0, sizeof(CHIP8));
	state->mem = save;
	state->flags = flags;
	state->PC = 0x200;
}

void CHIP8_deinit(CHIP8 *state)
{
	if (state->mem != NULL)
		free(state->mem);
}

int CHIP8_memload(CHIP8 *state, char *buf, size_t len)
{
	if (len > 0xFFF - 0x200 + 1)
		return CHIP8_TOOLARGE;
	memcpy(state->mem + 0x200, buf, len);
	return CHIP8_OK;
}

const char* CHIP8_error(int err)
{
	switch (err)
	{
	case CHIP8_MALLOC:
		return "Cannot allocate memory";
	case CHIP8_INVINST:
		return "Invalid instruction";
	case CHIP8_MEMACCV:
		return "Memory access violation";
	case CHIP8_STKFLOW:
		return "Stack under/overflow";
	case CHIP8_INVKEY:
		return "Invalid key";
	case CHIP8_TOOLARGE:
		return "Program too large";
	case CHIP8_EXIT:
		return "Program exited";
	default:
		return "";
	}
}

void CHIP8_dump(CHIP8 *state)
{
	printf("-------------------------------\n");
	printf("PC=0x%.3X  I=0x%.3X SP=0x%.3X\n", state->PC, state->I, state->SP);
	printf("  T=0x%.2X  ST=0x%.2X wait=%d\n\n", state->T, state->ST, state->waiting);
	printf("V0=0x%.2X V1=0x%.2X V2=0x%.2X V3=0x%.2X\n", state->V[0], state->V[1], state->V[2], state->V[3]);
	printf("V3=0x%.2X V5=0x%.2X V6=0x%.2X V7=0x%.2X\n", state->V[4], state->V[5], state->V[6], state->V[7]);
	printf("V8=0x%.2X V9=0x%.2X VA=0x%.2X VB=0x%.2X\n", state->V[8], state->V[9], state->V[10], state->V[11]);
	printf("VC=0x%.2X VD=0x%.2X VE=0x%.2X VF=0x%.2X\n", state->V[12], state->V[13], state->V[14], state->V[15]);
	printf("-------------------------------\n");
}

void CHIP8_bugset(CHIP8 *state, uint8_t flags)
{
	state->flags |= flags;
}

void CHIP8_bugclear(CHIP8 *state, uint8_t flags)
{
	state->flags &= ~flags;
}

void CHIP8_bugflip(CHIP8 *state, uint8_t flags)
{
	state->flags ^= flags;
}

int CHIP8_isbugset(CHIP8 *state, uint8_t flags)
{
	return state->flags & flags;
}

int CHIP8_keydown(CHIP8 *state, uint8_t key)
{
	if (state->waiting)
		return CHIP8_OK;
	if (key > 0xF)
		return CHIP8_INVKEY;

	state->keys[key] = 1;
	return CHIP8_OK;
}

int CHIP8_keyup(CHIP8 *state, uint8_t key)
{
	if (state->waiting)
		return CHIP8_OK;
	if (key > 0xF)
		return CHIP8_INVKEY;

	state->keys[key] = 0;
	return CHIP8_OK;
}

int CHIP8_keywait(CHIP8 *state, uint8_t key)
{
	if (!state->waiting)
		return CHIP8_OK;
	if (key > 0xF)
		return CHIP8_INVKEY;

	state->V[state->waitreg] = key;
	state->waiting = 0;
	return CHIP8_OK;
}

int CHIP8_pixelval(CHIP8 *state, uint8_t x, uint8_t y)
{
	if (x >= 128 || y >= 64)
		return CHIP8_MEMACCV;
	return state->display[x][y];
}

int CHIP8_extended(CHIP8 *state)
{
	return state->extended;
}

int CHIP8_waiting(CHIP8 *state)
{
	return state->waiting;
}

int CHIP8_playsnd(CHIP8 *state)
{
	return (state->ST > 0);
}

void CHIP8_tim(CHIP8 *state)
{
	if (state->T > 0)
		state->T--;
	if (state->ST > 0)
		state->ST--;
}

int CHIP8_cycle(CHIP8 *state)
{
	if (state->waiting)
		return CHIP8_OK;
	if (state->PC >= 0xFFF)
		return CHIP8_MEMACCV;

	uint16_t op = state->mem[state->PC] << 8 | state->mem[state->PC + 1];
	uint8_t upper = (op & 0xF000) >> 12;

	int ret;
	switch (upper)
	{
	case 0x0:	ret = op0(state, op);	break;
	case 0x1:	ret = op1(state, op);	break;
	case 0x2:	ret = op2(state, op);	break;
	case 0x3:	ret = op3(state, op);	break;
	case 0x4:	ret = op4(state, op);	break;
	case 0x5:	ret = op5(state, op);	break;
	case 0x6:	ret = op6(state, op);	break;
	case 0x7:	ret = op7(state, op);	break;
	case 0x8:	ret = op8(state, op);	break;
	case 0x9:	ret = op9(state, op);	break;
	case 0xA:	ret = opA(state, op);	break;
	case 0xB:	ret = opB(state, op);	break;
	case 0xC:	ret = opC(state, op);	break;
	case 0xD:	ret = opD(state, op);	break;
	case 0xE:	ret = opE(state, op);	break;
	case 0xF:	ret = opF(state, op);	break;
	}

	return ret;
}

//
//Opcode implementations
//
static int op0(CHIP8 *state, uint16_t op)	//Misc instructions: (0x00XN)
{
	uint8_t X = (op & 0xFFF0) >> 4;
	uint8_t N = op & 0xF;

	switch (X)
	{
	case 0x00C:	//(Super) Scroll screen N lines down
		for (int y = 63; y >= N; y--)
		{
			for (int x = 0; x < 128; x++)
			{
				state->display[x][y] = state->display[x][y - N];
			}
		}

		for (int y = 0; y < N; y++)
		{
			for (int x = 0; x < 128; x++)
			{
				state->display[x][y] = 0;
			}
		}
		break;

	case 0x00E:
		switch (N)
		{
		case 0x0:	//Clear screen
			memset(state->display, 0, sizeof(state->display));
			break;

		case 0xE:	//Return from subroutine
			if (state->SP == 0)	//Stack underflow
				return CHIP8_STKFLOW;
			state->PC = state->stk[state->SP];	//Pop address from stack
			state->SP--;
			break;

		default:	//Invalid instruction
			return CHIP8_INVINST;
		}
		break;

	case 0x00F:	//(Super) SuperChip extensions
		switch (N)
		{
		case 0xB:	//(Super) Scroll screen 4 columns right (2 columns in non-extended)
			for (int x = 127; x >= 4; x--)
			{
				for (int y = 0; y < 64; y++)
				{
					state->display[x][y] = state->display[x - 4][y];
				}
			}
			for (int x = 0; x < 4; x++)
			{
				for (int y = 0; y < 64; y++)
				{
					state->display[x][y] = 0;
				}
			}
			break;

		case 0xC:	//(Super) Scroll screen 4 columns left (2 columns in non-extended)
			for (int x = 0; x < 128 - 4; x++)
			{
				for (int y = 0; y < 64; y++)
				{
					state->display[x][y] = state->display[x + 4][y];
				}
			}
			for (int x = 127 - 4; x < 128; x++)
			{
				for (int y = 0; y < 64; y++)
				{
					state->display[x][y] = 0;
				}
			}
			break;

		case 0xD:	//(Super) Exit interpreter
			return CHIP8_EXIT;
			break;

		case 0xE:	//(Super) Disable extended graphics
			state->extended = 0;
			break;

		case 0xF:	//(Super) Enable extended graphics
			state->extended = 1;
			break;

		default:	//Invalid instruction
			return CHIP8_INVINST;
		}
		break;

	default:	//Invalid instruction
		return CHIP8_INVINST;
		break;
	}

	state->PC += 2;
	return CHIP8_OK;
}

static int op1(CHIP8 *state, uint16_t op)	//Absolute jump: (0x1NNN)
{
	state->PC = (op & 0xFFF);
	return CHIP8_OK;
}

static int op2(CHIP8 *state, uint16_t op)	//Jump to subroutine: (0x2NNN)
{
	if (state->SP == 15)	//Stack overflow
		return CHIP8_STKFLOW;
	state->SP++;
	state->stk[state->SP] = state->PC;	//Save PC value in stack
	state->PC = (op & 0xFFF);
	return CHIP8_OK;
}

static int op3(CHIP8 *state, uint16_t op)	//Skip next instruction if VX=NN: (0x3XNN)
{
	uint8_t X = (op & 0xF00) >> 8;
	uint8_t NN = op & 0xFF;

	if (state->V[X] == NN)
		state->PC += 4;
	else
		state->PC += 2;

	return CHIP8_OK;
}

static int op4(CHIP8 *state, uint16_t op)	//Skip next instruction if VX!=NN: (0x4XNN)
{
	uint8_t X = (op & 0xF00) >> 8;
	uint8_t NN = op & 0xFF;

	if (state->V[X] != NN)
		state->PC += 4;
	else
		state->PC += 2;

	return CHIP8_OK;
}

static int op5(CHIP8 *state, uint16_t op)	//Skip next instruction if VX=VY: (0x5XY0)
{
	uint8_t X = (op & 0xF00) >> 8;
	uint8_t Y = (op & 0xF0) >> 4;

	if (state->V[X] == state->V[Y])
		state->PC += 4;
	else
		state->PC += 2;

	return CHIP8_OK;
}

static int op6(CHIP8 *state, uint16_t op)	//Set VX to NN: (0x6XNN)
{
	uint8_t X = (op & 0xF00) >> 8;
	uint8_t NN = (op & 0xFF);

	state->V[X] = NN;
	state->PC += 2;

	return CHIP8_OK;
}

static int op7(CHIP8 *state, uint16_t op)	//Add NN to VX: (0x7XNN)
{
	uint8_t X = (op & 0xF00) >> 8;
	uint8_t NN = (op & 0xFF);

	state->V[X] += NN;
	state->PC += 2;

	return CHIP8_OK;
}

static int op8(CHIP8 *state, uint16_t op)	//Arithmetic operations: (0x8XYN)
{
	uint8_t X = (op & 0xF00) >> 8;
	uint8_t Y = (op & 0xF0) >> 4;
	uint8_t N = op & 0xF;
	uint8_t flag;	//Stores value of VF to store after operation
	uint8_t res;	//Stores result for opcodes that modify VF
	int reg;

	switch (N)
	{
	case 0x0:	//Set VX to VY
		state->V[X] = state->V[Y];
		break;

	case 0x1:	//Set VX to VX OR VY
		state->V[X] |= state->V[Y];
		break;

	case 0x2:	//Set VX to VX AND VY
		state->V[X] &= state->V[Y];
		break;

	case 0x3:	//Set VX to VX XOR VY
		state->V[X] ^= state->V[Y];
		break;

	case 0x4:	//Add VY to VX set V[F] to 1 if carry
		flag = ((state->V[X] + state->V[Y]) > 255) ? 1 : 0;
		res = state->V[X] + state->V[Y];

		state->V[X] = res;
		state->V[0xF] = flag;
		if (state->flags & CHIP8_BUG_VFFLAG)
			state->V[X] = res;
		break;

	case 0x5:	//Subtract VY from VX, V[F] = 0 if borrow
		flag = (state->V[Y] > state->V[X]) ? 0 : 1;
		res = state->V[X] - state->V[Y];

		state->V[X] = res;
		state->V[0xF] = flag;
		if (state->flags & CHIP8_BUG_VFFLAG)
			state->V[X] = res;
		break;

	case 0x6:	//Right shift VX, V[F] set to LSB before shift
		reg = (state->flags & CHIP8_BUG_SHIFTS) ? X : Y;
		flag = (state->V[reg] & 0x1) ? 1 : 0;
		res = state->V[reg] >> 1;

		state->V[X] = res;
		state->V[0xF] = flag;
		if (state->flags & CHIP8_BUG_VFFLAG)
			state->V[X] = res;
		break;

	case 0x7:	//Set VX to VY - VX, V[F] = 0 if borrow
		flag = (state->V[X] > state->V[Y]) ? 0 : 1;
		res = state->V[Y] - state->V[X];

		state->V[X] = res;
		state->V[0xF] = flag;
		if (state->flags & CHIP8_BUG_VFFLAG)
			state->V[X] = res;
		break;

	case 0xE:	//Left shift VX, V[F] set to MSB before shift
		reg = (state->flags & CHIP8_BUG_SHIFTS) ? X : Y;
		flag = (state->V[reg] & 0x80) ? 1 : 0;
		res = state->V[reg] << 1;

		state->V[X] = res;
		state->V[0xF] = flag;
		if (state->flags & CHIP8_BUG_VFFLAG)
			state->V[X] = res;
		break;

	default:	//Invalid instruction
		return CHIP8_INVINST;
		break;
	}

	state->PC += 2;
	return CHIP8_OK;
}

static int op9(CHIP8 *state, uint16_t op)	//Skip next instruction if VX!=VY: (0x9XY0)
{
	uint8_t X = (op & 0xF00) >> 8;
	uint8_t Y = (op & 0xF0) >> 4;

	if (state->V[X] != state->V[Y])
		state->PC += 4;
	else
		state->PC += 2;

	return CHIP8_OK;
}

static int opA(CHIP8 *state, uint16_t op)	//Set I to NNN: (0xANNN)
{
	state->I = (op & 0xFFF);
	state->PC += 2;
	return CHIP8_OK;
}

static int opB(CHIP8 *state, uint16_t op)	//Jump to NNN + V0: (0xBNNN)
{
	state->PC = (op & 0xFFF) + state->V[0];
	return CHIP8_OK;
}

static int opC(CHIP8 *state, uint16_t op)	//Set VX to random number and NN: (0xCXNN)
{
	uint8_t X = (op & 0xF00) >> 8;
	uint8_t NN = (op & 0xFF);

	state->V[X] = rand() & NN;
	state->PC += 2;

	return CHIP8_OK;
}

static int opD(CHIP8 *state, uint16_t op)	//Draw sprite at I at VX and VY: (0xDXYN)
{
	uint8_t X = (op & 0xF00) >> 8;
	uint8_t Y = (op & 0xF0) >> 4;
	uint8_t N = (op & 0xF);

	state->V[0xF] = 0;
	if (N == 0 && state->extended)	//(Super) 16x16 sprites
	{
		for (int row = 0; row < 16; row++)
		{
			if (state->I + row * 2 + 1 > 0xFFF)
				return CHIP8_MEMACCV;
			uint8_t upper = state->mem[state->I + row * 2];
			uint8_t lower = state->mem[state->I + row * 2 + 1];
			uint16_t sprite = (upper << 8) | lower;	//Sprites are 16 bits wide (1 row)
			uint16_t mask = 0x8000;

			for (int col = 0; col < 16; col++)	//For each bit in sprite row
			{
				uint8_t xval = (state->V[X] + col) % 128;
				uint8_t yval = (state->V[Y] + row) % 64;

				if ((sprite & mask) && state->display[xval][yval])
					state->V[0xF] = 1;

				state->display[xval][yval] ^= (sprite & mask) ? 1 : 0;
				mask >>= 1;
			}
		}
	}
	else	//Standard 8xN sprites
	{
		if (N == 0 && !state->extended)	//N=0 in 64x32 mode draws a 8x16 sprite
			N = 16;
		for (int row = 0; row < N; row++)
		{
			if (state->I + row > 0xFFF)
				return CHIP8_MEMACCV;
			uint8_t sprite = state->mem[state->I + row];	//Sprites are 8 bits wide (1 row)
			uint8_t mask = 0x80;

			for (int col = 0; col < 8; col++)	//For each bit in sprite row
			{
				if (state->extended)	//(Super) 128x64
				{
					uint8_t xval = (state->V[X] + col) % 128;
					uint8_t yval = (state->V[Y] + row) % 64;

					if ((sprite & mask) && state->display[xval][yval])
						state->V[0xF] = 1;

					state->display[xval][yval] ^= (sprite & mask) ? 1 : 0;
					mask >>= 1;
				}
				else //64x32 (2x2 pixels on 128x64)
				{
					uint8_t xval = ((state->V[X] + col) * 2) % 128;
					uint8_t yval = ((state->V[Y] + row) * 2) % 64;

					if ((sprite & mask) && state->display[xval][yval])
						state->V[0xF] = 1;

					//2x2 pixel
					state->display[xval][yval] ^= (sprite & mask) ? 1 : 0;
					state->display[xval + 1][yval] = state->display[xval][yval];
					state->display[xval][yval + 1] = state->display[xval][yval];
					state->display[xval + 1][yval + 1] = state->display[xval][yval];
					mask >>= 1;
				}
			}
		}
	}

	state->PC += 2;
	return CHIP8_OK;
}

static int opE(CHIP8 *state, uint16_t op)	//Input operations: (0xEXNN)
{
	uint8_t X = (op & 0xF00) >> 8;
	uint8_t NN = (op & 0xFF);

	if (state->V[X] > 15)	//Only 16 keys
		return CHIP8_INVKEY;

	switch (NN)
	{
	case 0x9E:	//Skip next instruction if key in VX is pressed
		if (state->keys[state->V[X]] == 1)
			state->PC += 4;
		else
			state->PC += 2;
		break;

	case 0xA1:	//Skip next instruction if key in VX is not pressed
		if (state->keys[state->V[X]] != 1)
			state->PC += 4;
		else
			state->PC += 2;
		break;

	default:	//Invalid instruction
		return CHIP8_INVINST;
		break;
	}
	return CHIP8_OK;
}

static int opF(CHIP8 *state, uint16_t op)	//Misc instructions: (0xFXNN)
{
	uint8_t X = (op & 0xF00) >> 8;
	uint8_t NN = (op & 0xFF);

	switch (NN)
	{
	case 0x07:	//Sets VX to value of the delay timer
		state->V[X] = state->T;
		break;

	case 0x0A:	//Waits for a keypress
		state->waiting = 1;
		state->waitreg = X;
		break;

	case 0x15:	//Sets delay timer to VX
		state->T = state->V[X];
		break;

	case 0x18:	//Sets sound timer to VX
		state->ST = state->V[X];
		break;

	case 0x1E:	//Add VX to I
		state->I += state->V[X];
		break;

	case 0x29:	//Sets I to location of sprite for character in VX
		state->I = ((state->V[X]) * 5);
		break;

	case 0x30:	//(Super) Sets I to location of 10-bit sprite for number in VX
		state->I = ((state->V[X]) * 10) + sizeof(font);
		break;

	case 0x33:	//Stores the BCD representation of VX in address pointed by I
		if (state->I + 2 > 0xFFF)
			return CHIP8_MEMACCV;
		state->mem[state->I] = state->V[X] / 100;				//Hundreds
		state->mem[state->I + 1] = (state->V[X] / 10) % 10;		//Tens
		state->mem[state->I + 2] = (state->V[X] % 100) % 10;	//Ones
		break;

	case 0x55:	//Stores V0 to VX in address pointed by I
		if (state->I + X > 0xFFF)
			return CHIP8_MEMACCV;
		for (int i = 0; i <= X; i++)
		{
			state->mem[state->I + i] = state->V[i];
		}
		if (!(state->flags & CHIP8_BUG_LOADSTORE))
			state->I += X + 1;
		break;

	case 0x65:	//Restores V0 to VX from address pointed by I
		if (state->I + X > 0xFFF)
			return CHIP8_MEMACCV;
		for (int i = 0; i <= X; i++)
		{
			state->V[i] = state->mem[state->I + i];
		}
		if (!(state->flags & CHIP8_BUG_LOADSTORE))
			state->I += X + 1;
		break;

	case 0x75:	//(Super) Store registers V0 to VX (X<=7) in RPL flags
		if (X > 7)
			return CHIP8_MEMACCV;
		for (int i = 0; i <= X; i++)
		{
			state->RPL[i] = state->V[i];
		}
		break;

	case 0x85:	//(Super) Restore registers V0 to VX (X<=7) in RPL flags
		if (X > 7)
			return CHIP8_MEMACCV;
		for (int i = 0; i <= X; i++)
		{
			state->V[i] = state->RPL[i];
		}
		break;

	default:	//Invalid instruction
		return CHIP8_INVINST;
		break;
	}

	state->PC += 2;
	return CHIP8_OK;
}
