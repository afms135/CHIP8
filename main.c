#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <time.h>
#ifdef WIN32
#include <SDL.h>
#else
#include <SDL2/SDL.h>
#endif
#include "CHIP8.h"

#define FREQUENCY 1000
#define VOLUME 5	//0-127
#define SAMPLERATE 44100
#define SAMPLESCYCLE (SAMPLERATE/FREQUENCY)	//number of samples in 1 cycle
#define SAMPLESHIGH (SAMPLESCYCLE/2)	//Number of samples in half a cycle

int load(CHIP8 *state, char *file);
void draw(CHIP8 *state, SDL_Texture *tex);
void input(CHIP8 *state, SDL_Event event, int *mute);
void callback(void* userdata, Uint8* stream, int length);

int main(int argc, char* argv[])
{
	if (argc < 2)
	{
		fprintf(stderr, "Usage: CHIP8 <file>\n");
		fprintf(stderr, "Keyboard Mapping:\n");
		fprintf(stderr, "RETURN: Resets emulator\n");
		fprintf(stderr, "     m: Mutes/Unmutes emulator\n");
		fprintf(stderr, "     0: Toggles Shift Bug      (Default ON)\n");
		fprintf(stderr, "     -: Toggles Load Store Bug (Default ON)\n");
		fprintf(stderr, "     =: Toggles VF Flag Bug    (Default OFF)\n");
		fprintf(stderr, "Keypad Map:\n");
		fprintf(stderr, "1 2 3 4  ->  1 2 3 C\n");
		fprintf(stderr, "Q W E R  ->  4 5 6 D\n");
		fprintf(stderr, "A S D F  ->  7 8 9 E\n");
		fprintf(stderr, "Z X C V  ->  A 0 B F\n");
		return -1;
	}

	//
	//	Init CHIP-8 emulator
	//
	CHIP8 state;

	int ret = CHIP8_init(&state);
	if (ret)
	{
		fprintf(stderr, "Error: CHIP8_init %s\n", CHIP8_error(ret));
		return -1;
	}

	if (load(&state, argv[1]))
		return 1;

	CHIP8_bugset(&state, CHIP8_BUG_SHIFTS | CHIP8_BUG_LOADSTORE);	//Most games rely on these bugs

	//
	//	Init SDL, rendering and audio subsystems
	//
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) != 0)
	{
		fprintf(stderr, "Error: SDL_Init %s\n", SDL_GetError());
		return -1;
	}

	SDL_Window *window = SDL_CreateWindow("CHIP-8", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 640, 320, 0);
	if (window == NULL)
	{
		fprintf(stderr, "Error: SDL_CreateWindow %s\n", SDL_GetError());
		return -1;
	}

	SDL_Renderer *render = SDL_CreateRenderer(window, -1, 0);
	if (render == NULL)
	{
		fprintf(stderr, "Error: SDL_CreateRenderer %s\n", SDL_GetError());
		return -1;
	}

	SDL_Texture *tex = SDL_CreateTexture(render, SDL_PIXELFORMAT_RGB565, SDL_TEXTUREACCESS_STREAMING, 128, 64);
	if (tex == NULL)
	{
		fprintf(stderr, "Error: SDL_CreateTexture %s\n", SDL_GetError());
		return -1;
	}

	int mute;	//0=unmute, 1=mute, 2=no audio device
	SDL_AudioSpec spec;
	spec.freq = SAMPLERATE;
	spec.format = AUDIO_S8;
	spec.channels = 1;
	spec.samples = 2048;
	spec.callback = callback;

	SDL_AudioDeviceID aud = SDL_OpenAudioDevice(NULL, 0, &spec, NULL, 0);
	if (aud == 0)
	{
		mute = 2;
		fprintf(stderr, "Error: SDL_OpenAudioDevice %s\n", SDL_GetError());
	}
	else
	{
		mute = 0;
		SDL_PauseAudioDevice(aud, 1);
	}

	//
	//	Start emulation
	//
	SDL_Event Event;
	int run = 1;

	while (run)
	{
		Uint32 framestart = SDL_GetTicks();

		while (SDL_PollEvent(&Event))
		{
			if (Event.type == SDL_QUIT)
				run = 0;
			else if (Event.type == SDL_DROPFILE)
			{
				char *file = Event.drop.file;
				if (load(&state, file))
					break;
				CHIP8_reset(&state);
				SDL_free(file);
			}
			else
				input(&state, Event, &mute);
		}

		int speed = (CHIP8_extended(&state)) ? 20 : 10;
		for (int i = 0; i < speed; i++)
		{
			int ret = CHIP8_cycle(&state);
			if (ret == CHIP8_EXIT)
			{
				run = 0;
				break;
			}
			else if (ret != CHIP8_OK)
			{
				fprintf(stderr, "Error: CHIP8_cycle %s\n", CHIP8_error(ret));
				CHIP8_dump(&state);
				SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Error: CHIP8_cycle", CHIP8_error(ret), NULL);
				run = 0;
				break;
			}
		}

		CHIP8_tim(&state);

		if (mute)
		{
			if (aud != 0)
				SDL_PauseAudioDevice(aud, 1);
		}
		else
		{
			if (CHIP8_playsnd(&state))
				SDL_PauseAudioDevice(aud, 0);
			else
				SDL_PauseAudioDevice(aud, 1);
		}

		draw(&state, tex);
		SDL_RenderCopy(render, tex, NULL, NULL);
		SDL_RenderPresent(render);

		Uint32 frameend = SDL_GetTicks();
		if (frameend - framestart < (1000 / 60))
			SDL_Delay(16 - (frameend - framestart));
	}

	//
	//	Cleanup
	//
	CHIP8_deinit(&state);
	SDL_CloseAudioDevice(aud);
	SDL_DestroyTexture(tex);
	SDL_DestroyRenderer(render);
	SDL_DestroyWindow(window);
	SDL_Quit();
	return 0;
}

int load(CHIP8 *state, char *file)
{
	FILE *program = fopen(file, "rb");
	if (program == NULL)
	{
		fprintf(stderr, "Error: fopen %s %s\n", file, strerror(errno));
		return -1;
	}

	fseek(program, 0, SEEK_END);
	long size = ftell(program);	//Get file size
	fseek(program, 0, SEEK_SET);

	char buf[CHIP8_MAXPROGSIZE];
	fread(buf, 1, CHIP8_MAXPROGSIZE, program);

	if (ferror(program))
	{
		fprintf(stderr, "Error: fread %s\n", strerror(errno));
		fclose(program);
		return -1;
	}
	fclose(program);

	int ret = CHIP8_memload(state, buf, size);
	if (ret)
	{
		fprintf(stderr, "Error: CHIP8_memload %s\n", CHIP8_error(ret));
		return -1;
	}

	return 0;
}

void draw(CHIP8 *state, SDL_Texture *tex)
{
	void *pixels;
	int pitch;

	SDL_LockTexture(tex, NULL, &pixels, &pitch);

	unsigned short *pix = (unsigned short*)pixels;

	for (int x = 0; x < 128; x++)
	{
		for (int y = 0; y < 64; y++)
		{
			pix[x + (y * 128)] = (CHIP8_pixelval(state, x, y)) ? 0xFFFF : 0;
		}
	}

	SDL_UnlockTexture(tex);
}

void callback(void* userdata, Uint8* stream, int length)
{
	static int ptr = 0;	//Current location in a full cycle

	for (int i = 0; i < length; i++)
	{
		if (ptr < SAMPLESHIGH)
			stream[i] = VOLUME;
		else
			stream[i] = -VOLUME;

		ptr++;

		if (ptr >= SAMPLESCYCLE)
			ptr = 0;
	}
}

void input(CHIP8 *state, SDL_Event event, int *mute)
{
	switch (event.type)
	{
	case SDL_KEYDOWN:
		if (!CHIP8_waiting(state))
		{
			switch (event.key.keysym.scancode)
			{
			case SDL_SCANCODE_RETURN:
				CHIP8_reset(state);
				break;

			case SDL_SCANCODE_M:
				if (*mute == 2)	//No audio device
					break;
				else
					*mute ^= 1;	//Flip mute status
				break;

			case SDL_SCANCODE_0:
				CHIP8_bugflip(state, CHIP8_BUG_SHIFTS);
				if (CHIP8_isbugset(state, CHIP8_BUG_SHIFTS))
					printf("Shift Bug: ON\n");
				else
					printf("Shift Bug: OFF\n");
				break;

			case SDL_SCANCODE_MINUS:
				CHIP8_bugflip(state, CHIP8_BUG_LOADSTORE);
				if (CHIP8_isbugset(state, CHIP8_BUG_LOADSTORE))
					printf("Load Store Bug: ON\n");
				else
					printf("Load Store Bug: OFF\n");
				break;

			case SDL_SCANCODE_EQUALS:
				CHIP8_bugflip(state, CHIP8_BUG_VFFLAG);
				if (CHIP8_isbugset(state, CHIP8_BUG_VFFLAG))
					printf("VF Flag Bug: ON\n");
				else
					printf("VF Flag Bug: OFF\n");
				break;

			case SDL_SCANCODE_1:	CHIP8_keydown(state, 0x1);	break;	//1
			case SDL_SCANCODE_2:	CHIP8_keydown(state, 0x2);	break;	//2
			case SDL_SCANCODE_3:	CHIP8_keydown(state, 0x3);	break;	//3
			case SDL_SCANCODE_4:	CHIP8_keydown(state, 0xC);	break;	//C
			case SDL_SCANCODE_Q:	CHIP8_keydown(state, 0x4);	break;	//4
			case SDL_SCANCODE_W:	CHIP8_keydown(state, 0x5);	break;	//5
			case SDL_SCANCODE_E:	CHIP8_keydown(state, 0x6);	break;	//6
			case SDL_SCANCODE_R:	CHIP8_keydown(state, 0xD);	break;	//D
			case SDL_SCANCODE_A:	CHIP8_keydown(state, 0x7);	break;	//7
			case SDL_SCANCODE_S:	CHIP8_keydown(state, 0x8);	break;	//8
			case SDL_SCANCODE_D:	CHIP8_keydown(state, 0x9);	break;	//9
			case SDL_SCANCODE_F:	CHIP8_keydown(state, 0xE);	break;	//E
			case SDL_SCANCODE_Z:	CHIP8_keydown(state, 0xA);	break;	//A
			case SDL_SCANCODE_X:	CHIP8_keydown(state, 0x0);	break;	//0
			case SDL_SCANCODE_C:	CHIP8_keydown(state, 0xB);	break;	//B
			case SDL_SCANCODE_V:	CHIP8_keydown(state, 0xF);	break;	//F
			default:	break;
			}
		}
		else
		{
			switch (event.key.keysym.scancode)
			{
			case SDL_SCANCODE_1:	CHIP8_keywait(state, 0x1);	break;	//1
			case SDL_SCANCODE_2:	CHIP8_keywait(state, 0x2);	break;	//2
			case SDL_SCANCODE_3:	CHIP8_keywait(state, 0x3);	break;	//3
			case SDL_SCANCODE_4:	CHIP8_keywait(state, 0xC);	break;	//C
			case SDL_SCANCODE_Q:	CHIP8_keywait(state, 0x4);	break;	//4
			case SDL_SCANCODE_W:	CHIP8_keywait(state, 0x5);	break;	//5
			case SDL_SCANCODE_E:	CHIP8_keywait(state, 0x6);	break;	//6
			case SDL_SCANCODE_R:	CHIP8_keywait(state, 0xD);	break;	//D
			case SDL_SCANCODE_A:	CHIP8_keywait(state, 0x7);	break;	//7
			case SDL_SCANCODE_S:	CHIP8_keywait(state, 0x8);	break;	//8
			case SDL_SCANCODE_D:	CHIP8_keywait(state, 0x9);	break;	//9
			case SDL_SCANCODE_F:	CHIP8_keywait(state, 0xE);	break;	//E
			case SDL_SCANCODE_Z:	CHIP8_keywait(state, 0xA);	break;	//A
			case SDL_SCANCODE_X:	CHIP8_keywait(state, 0x0);	break;	//0
			case SDL_SCANCODE_C:	CHIP8_keywait(state, 0xB);	break;	//B
			case SDL_SCANCODE_V:	CHIP8_keywait(state, 0xF);	break;	//F
			default:	break;
			}
		}
		break;
	case SDL_KEYUP:
		switch (event.key.keysym.scancode)
		{
		case SDL_SCANCODE_1:	CHIP8_keyup(state, 0x1);	break;	//1
		case SDL_SCANCODE_2:	CHIP8_keyup(state, 0x2);	break;	//2
		case SDL_SCANCODE_3:	CHIP8_keyup(state, 0x3);	break;	//3
		case SDL_SCANCODE_4:	CHIP8_keyup(state, 0xC);	break;	//C
		case SDL_SCANCODE_Q:	CHIP8_keyup(state, 0x4);	break;	//4
		case SDL_SCANCODE_W:	CHIP8_keyup(state, 0x5);	break;	//5
		case SDL_SCANCODE_E:	CHIP8_keyup(state, 0x6);	break;	//6
		case SDL_SCANCODE_R:	CHIP8_keyup(state, 0xD);	break;	//D
		case SDL_SCANCODE_A:	CHIP8_keyup(state, 0x7);	break;	//7
		case SDL_SCANCODE_S:	CHIP8_keyup(state, 0x8);	break;	//8
		case SDL_SCANCODE_D:	CHIP8_keyup(state, 0x9);	break;	//9
		case SDL_SCANCODE_F:	CHIP8_keyup(state, 0xE);	break;	//E
		case SDL_SCANCODE_Z:	CHIP8_keyup(state, 0xA);	break;	//A
		case SDL_SCANCODE_X:	CHIP8_keyup(state, 0x0);	break;	//0
		case SDL_SCANCODE_C:	CHIP8_keyup(state, 0xB);	break;	//B
		case SDL_SCANCODE_V:	CHIP8_keyup(state, 0xF);	break;	//F
		default:	break;
		}
		break;
	}
}
