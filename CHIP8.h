#pragma once
#include <stdlib.h>
#include <stdint.h>

//
//	CHIP-8 state
//
typedef struct CHIP8
{
	uint8_t *mem;
	uint8_t display[128][64];
	uint8_t extended;
	uint8_t keys[16];
	uint8_t RPL[7];
	uint8_t flags;

	uint8_t waiting;
	uint8_t waitreg;

	uint16_t PC;
	uint16_t I;
	uint8_t V[16];
	uint8_t T;
	uint8_t ST;

	uint8_t SP;
	uint16_t stk[16];
} CHIP8;

//
//	Generic defines
//
#define CHIP8_MAXPROGSIZE	(0xFFF - 0x200 + 1)

//
//	Error codes
//
#define CHIP8_OK 0
#define CHIP8_INVINST	-1	//Invalid instruction
#define CHIP8_MALLOC	-2	//Memory allocation error
#define CHIP8_STKFLOW	-3	//Stack over/underflow
#define CHIP8_MEMACCV	-4	//Memory access violation
#define CHIP8_INVKEY	-5	//Invalid key
#define CHIP8_TOOLARGE	-6	//Program too large
#define CHIP8_EXIT		-7	//Execution stopped

//
//	Interpreter bug flags
//
#define CHIP8_BUG_SHIFTS	(1<<0)
#define CHIP8_BUG_LOADSTORE (1<<1)
#define CHIP8_BUG_VFFLAG	(1<<2)

//
//	Initializes CHIP-8 state struct
//
int CHIP8_init(CHIP8 *state);

//
//	Resets the CHIP-8 state
//
void CHIP8_reset(CHIP8 *state);

//
//	Deallocates CHIP-8 state struct
//
void CHIP8_deinit(CHIP8 *state);

//
//	Loads program of size len in buf into CHIP-8 memory
//
int CHIP8_memload(CHIP8 *state, char *buf, size_t len);

//
//	Returns string describing error number, returns empty string if unknown
//
const char* CHIP8_error(int err);

//
//	Prints CHIP-8 internal state to stdout
//
void CHIP8_dump(CHIP8 *state);

//
//	Sets bug flag(s)
//
void CHIP8_bugset(CHIP8 *state, uint8_t flags);

//
//	Clears bug flag(s)
//
void CHIP8_bugclear(CHIP8 *state, uint8_t flags);

//
//	Toggles bug flag(s)
//
void CHIP8_bugflip(CHIP8 *state, uint8_t flags);

//
//	Returns 1 if bug flag(s) are set, 0 otherwise
//
int CHIP8_isbugset(CHIP8 *state, uint8_t flags);

//
//	Preformes one cycle of CHIP-8, returns if waiting for a key press
//
int CHIP8_cycle(CHIP8 *state);

//
//	Updates CHIP-8 timers, should be called at 60Hz intervals
//
void CHIP8_tim(CHIP8 *state);

//
//	Returns 1 if waiting for a keypress, 0 otherwise
//
int CHIP8_waiting(CHIP8 *state);

//
//	Returns 1 if sound should be played, 0 otherwise
//
int CHIP8_playsnd(CHIP8 *state);

//
//	Returns 1 if extended graphics mode is enabled, 0 otherwise
//
int CHIP8_extended(CHIP8 *state);

//
//	Returns 1 if pixel set, 0 if not, CHIP8_MEMACCV if invalid co-ordinate
//
int CHIP8_pixelval(CHIP8 *state, uint8_t x, uint8_t y);

//
//	Signals a key press to CHIP-8, does nothing if waiting for a keypress
//
int CHIP8_keydown(CHIP8 *state, uint8_t key);

//
//	Signals a key release to CHIP-8, does nothing if waiting for a keypress
//
int CHIP8_keyup(CHIP8 *state, uint8_t key);

//
//	Signals a key press when CHIP-8 is waiting for one, does nothing otherwise
//
int CHIP8_keywait(CHIP8 *state, uint8_t key);






